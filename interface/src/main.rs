use std::path::Path;

use tide::Request;
use tide::prelude::*;

#[tokio::main]
async fn main() -> tide::Result<()> {
    let mut app = tide::new();
    app.at("/image/:filename").get(get_image);
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}

async fn get_index(req: Request<()>) -> tide::Result {
    Ok(format!("Index request").into())
}

async fn get_image(req: Request<()>) -> tide::Result {
    let filename = req.param("filename")?;

    Ok(format!("Got request for {:?}", filename).into())
}

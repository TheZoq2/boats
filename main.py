# import numpy as np
import pdb
import numpy as np
import cv2
import tracking
from datetime import datetime

from typing import Tuple

IMAGE_SCALE = 0.5

def rectify(frame):
    """
    Flips and scales the input image
    """
    small = cv2.resize(frame, (0,0), fx = IMAGE_SCALE, fy = IMAGE_SCALE)
    return cv2.flip(small, -1)


def optical_flow(prev, current, old_flow):
    """
    Calculates the optical flow between prev and current.

    Returns the flow
    """
    prev_gray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    current_gray = cv2.cvtColor(current, cv2.COLOR_BGR2GRAY)

    prev_smooth = smooth_image(prev_gray)
    current_smooth = smooth_image(current_gray)

    cv2.imshow("prev_smooth", prev_smooth)

    pyramid_scale = 0.5
    levels = 1
    winsize = 5
    iterations = 3
    poly_n = 5
    poly_sigma = 1.1
    flags = 0

    return cv2.calcOpticalFlowFarneback(
        prev_smooth,
        current_smooth,
        old_flow,
        pyramid_scale,
        levels,
        winsize,
        iterations,
        poly_n,
        poly_sigma,
        flags
    )


def smooth_image(frame):
    kernel_size = 7
    return cv2.GaussianBlur(frame, (kernel_size,kernel_size), 25)



def threshold_optical_flow(flow):
    """
    Thresholds the optical flow to remove small values
    """
    output = np.zeros(flow.shape[0:2])
    output[...] = flow[..., 1]**2 + flow[..., 0]**2 > 0.7
    return output



def process_morphologically(flow):
    open_kernel = np.ones((10,10))
    opened = cv2.morphologyEx(flow, cv2.MORPH_OPEN, open_kernel)
    close_kernel = np.ones((6, 6))
    return cv2.morphologyEx(opened, cv2.MORPH_CLOSE, close_kernel)


class ConnectedComponentOutput:
    def __init__(self, amount, labeled, locations):
        self.amount = amount
        self.labeled = labeled
        self.locations = locations


def connected_components(flow):
    """
    Finds connected components in the flow image.

    Returns a ConnectedComponentOutput
    """
    flow=flow.astype(np.uint8)
    (component_amount, markers, stats, centroids) = cv2.connectedComponentsWithStats(flow, ltype=cv2.CV_16U)

    locations = list(centroids[:])

    return ConnectedComponentOutput(component_amount, markers, locations)





def show_optical_flow(image_shape, flow):
    rgb = np.zeros(image_shape)
    rgb[..., 2] = 0

    rgb[..., 0] = flow[...,0]
    rgb[..., 1] = flow[...,1]

    cv2.imshow('flow',rgb)



def draw_locations_on_image(image, locations):
    drawn = np.copy(image)

    for location in locations:
        # cv2.circle(drawn, location, 5, (0, 0, 255), -1)
        cv2.circle(drawn, tuple(np.uint16(location)), 5, (0, 0, 255))

    return drawn


def crop_uninteresting(frame):
    return frame[600:900, 640:1280]


def show_connected_component_output(cc_output):
    label_hue = np.uint8(
        179 * cc_output.labeled / np.max(cc_output.labeled)
    )
    blank_ch = 255 * np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])
    # Set pixels only containing background to black
    labeled_img[label_hue == 0] = 0

    cv2.imshow("markers", cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR))


def draw_chains(target, chains):
    for chain in chains:
        # Don't draw points that just appeared, they are probably trash
        if len(chain) - tracking.count_trailing_none(chain) < 20:
            continue
        chain_without_none = list(filter(lambda x: x is not None, chain))
        segments = zip(chain_without_none, [chain_without_none[0]] + chain_without_none)

        for (start, end) in segments:
            cv2.line(target, tuple(np.uint16(start)), tuple(np.uint16(end)), (255,0,255), 1)

        velocity = tracking.calculate_average_velocity(chain, 30)
        if velocity is not None:
            # velocity = velocity * tracking.count_trailing_none(chain)
            end_point = np.uint16(chain_without_none[-1])
            estimated_current = tracking.estimate_current_position(chain, velocity)
            cv2.line(target, tuple(end_point), tuple(np.uint16(estimated_current)), (0, 255, 0))

            cv2.circle(
                target,
                tuple(np.uint16(estimated_current)),
                max_distance_fn(tracking.count_trailing_none(chain)),
                (0, 255, 0)
            )


################################################################################
# Configuration
def max_distance_fn(time_missing):
    return 10 + time_missing * 1

max_missing_frames = 20

# The edges which a chain must cross to count as a boat
count_area_start = 100
count_area_end = 150

################################################################################
def run_on_video(filename: str):
    """
        Main function
    """
    cap = cv2.VideoCapture(filename)

    prev = None
    old_flow = None


    chains = []

    i = 0
    while cap.isOpened():
        i += 1

        # Read data from the video and quit when there is no more data
        _ret, frame = cap.read()
        if frame is None:
            break

        # Skip every n frames
        if (i % 15) != 0:
            continue

        # Crop, rotate and scale frame
        frame = crop_uninteresting(frame)
        rectified = rectify(frame)

        # Optical flow can only run if there are 2 frames available
        if not prev is None:
            # Perform image processing chain
            flow = optical_flow(prev, rectified, old_flow)
            show_optical_flow(rectified.shape, flow)
            thresholded = threshold_optical_flow(flow)
            opened = process_morphologically(thresholded)
            cc_output = connected_components(opened)

            # Track potential boats
            chains = tracking.assign_points_to_chains(
                cc_output.locations,
                chains,
                max_distance_fn,
                max_missing_frames
            )

            # We count chains as new if they are 
            has_new_chains = any(map(lambda c: len(c) == 20, chains))

            if has_new_chains:
                # cv2.imwrite(frame, datetime.now())
                filename = datetime.now().strftime("%Y%m%d-%H%M%S-%f.png")
                print(f"Saving new boat as {filename}")
                cv2.imwrite(filename, frame)

            # Draw the output
            cv2.imshow("opened", opened)
            show_connected_component_output(cc_output)
            drawn = draw_locations_on_image(rectified, cc_output.locations)
            drawn = cv2.line(drawn, (count_area_start, 0), (count_area_start, drawn.shape[1]), (255, 0, 0));
            drawn = cv2.line(drawn, (count_area_end, 0), (count_area_end, drawn.shape[1]), (255, 0, 0));
            draw_chains(drawn, chains)
            cv2.imshow('drawn', cv2.resize(drawn, (0,0), fx = 2.5, fy = 2.5))

            old_flow = flow

        prev = rectified

        while cv2.waitKey(1) & 0xff == ord('q'):
            break


    cap.release()


def main():
    files = ["many_boats.mp4", "beating.mp4", "boat1.mp4", "boat2.avi", "lyxboat1.mp4", ]

    for file in files:
        run_on_video(file)

    # cap.destroyAllWindows()

if __name__ == "__main__":
    main()

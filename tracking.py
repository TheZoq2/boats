import numpy as np
from scipy.optimize import linear_sum_assignment
import unittest
import copy


def remove_leading_none(list):
    start_index = 0
    for (i, elem) in enumerate(list):
        start_index = i
        if elem is not None:
            break

    return list[start_index:]

def count_trailing_none(list):
    count = 0
    for elem in list[::-1]:
        if elem is None:
            count += 1
        else:
            break
    return count

def last_not_none(list):
    for elem in list[::-1]:
        if elem is not None:
            return elem
    return None





def calculate_average_velocity(points, max_chain_length):
    interesting_points = remove_leading_none(points[-max_chain_length:])

    # If there arent enough points to do the calculation
    if len(interesting_points) <= 1:
        return None

    velocity = np.zeros(2)
    prev_point = interesting_points[0]
    last_velocity = velocity

    # Calculate and sum the velocities
    for point in interesting_points[1:]:
        if point is not None:
            if prev_point is None:
                prev_point = point
            else:
                velocity += point - prev_point
                last_velocity = (point - prev_point)
                prev_point = point
        else:
            velocity += last_velocity

    # Make it an average
    return velocity / (len(interesting_points) - 1)

def estimate_current_position(chain, velocity):
    last_point = last_not_none(chain)
    return last_point + velocity * (count_trailing_none(chain) + 1)


def calculate_distance_matrix(points, old_chains, velocity_backtracking):
    velocities = []
    for old_points in old_chains:
        velocity = calculate_average_velocity(old_points, velocity_backtracking)
        velocities.append(velocity if not velocity is None else np.zeros(2))

    # Calculate the distance between the expected location of each chain
    # and the new points
    distances = []
    for (i, point) in enumerate(points):
        distances.append([])
        for (j, velocity) in enumerate(velocities):
            distances[i].append(
                np.linalg.norm(estimate_current_position(old_chains[j], velocity) - point)
            )

    # create a numpy matrix from the distances
    return np.array(distances)


def filter_distant_matches(point_ind, chain_ind, distances, time_missing, max_distance_fn):
    """
    Removes assignments from the hungarian method where the distance
    between the matched points is too large
    """
    def filter_fn(pair):
        (p, c) = pair
        return distances[p, c] < max_distance_fn(time_missing[c])

    # This list conversion is important because otherwise the second
    # list builder in the return statement gets a depleted iterator
    result = list(filter(filter_fn, zip(point_ind, chain_ind)))

    # Convoluted unzip because python
    return ([a for a,b in result], [b for a,b in result])


def assign_points_to_chains(points, old_chains, max_distance_fn, max_missing_frames):
    """
    Assigns the given list of points to the point chains provided. A new chain
    gets created from any points that did not get a matching chain

    Returns a new list of chains
    """
    distances = calculate_distance_matrix(points, old_chains, max_missing_frames)

    time_missing = list(map(count_trailing_none, old_chains))

    # Find the best assignment using the hungarian method
    (point_ind, chain_ind) = linear_sum_assignment(distances)
    (point_ind, chain_ind) = filter_distant_matches(point_ind, chain_ind, distances, time_missing, max_distance_fn)

    # Make a copy of the chains
    chains = copy.deepcopy(old_chains)

    # Put the new points into the corresponding chains
    for (i, chain) in enumerate(chain_ind):
        chains[chain].append(points[point_ind[i]])


    # Check which chains were unmatched and need to be removed
    chains = [ chain if i in chain_ind else chain + [None]
                for (i, chain) in enumerate(chains)
                if count_trailing_none(chain) < max_missing_frames
            ]
    # Check which points were unmatched and need a new chain
    new_points = [[point] for (i, point) in enumerate(points) if i not in point_ind]

    # Add the unmatched points to the chain
    chains += new_points

    return chains

################################################################################
#                           Tests
################################################################################

class InitialNoneTrimingTests(unittest.TestCase):
    def test_only_initial_none(self):
        self.assertEqual([1,2,3], remove_leading_none([None, None, 1,2,3]))

    def test_count_trailing_none(self):
        self.assertEqual(3, count_trailing_none([1,2,None, None, None]))

    def test_last_not_None(self):
        self.assertEqual(3, last_not_none([1,None,3,None,None]))

class VelocityTests(unittest.TestCase):
    def test_linear_velocity(self):
        points = [np.array([0,0]), np.array([0,1]), np.array([0,2]), np.array([0,3])]
        velocity = calculate_average_velocity(points, 4)
        np.testing.assert_array_almost_equal(velocity, np.array([0,1]))

    def test_velocity_of_single_point(self):
        points = [np.array([0,0])]
        self.assertEqual(calculate_average_velocity(points, 4), None)


    def test_velocity_long_chain(self):
        points = [np.array([0,0]), np.array([0,1]), np.array([0,3]), np.array([0,5])]
        velocity = calculate_average_velocity(points, 2)
        np.testing.assert_array_almost_equal(velocity, np.array([0,2]))

    def test_missing_velocities_work(self):
        points = [np.array([0,0]), None, None, np.array([0,3])]
        velocity = calculate_average_velocity(points, 4)
        np.testing.assert_array_almost_equal(velocity, np.array([0,1]))

    def test_initial_missing_velocities_work(self):
        points = [None, None, np.array([0,2]), np.array([0,3])]
        velocity = calculate_average_velocity(points, 4)
        np.testing.assert_array_almost_equal(velocity, np.array([0,1]))

    def test_trailing_nones_are_no_problem(self):
        points = [np.array([0,1]), np.array([0,2]), None, None]
        velocity = calculate_average_velocity(points, 4)
        np.testing.assert_array_almost_equal(velocity, np.array([0, 1]))


class ChainAssignmentTests(unittest.TestCase):
    def test_simple_assignment(self):
        points = [np.array([5,0]), np.array([5, 5]), np.array([0, 5])]
        old_chains = [
                [np.array([0, 3]), np.array([0,4])],
                [np.array([3, 0]), np.array([4,0])],
                [np.array([3, 3]), np.array([4,4])],
            ]

        new_chains = assign_points_to_chains(points, old_chains, lambda x: 2, 4)

        np.testing.assert_array_almost_equal(
            new_chains,
            [
                [np.array([0, 3]), np.array([0,4]), np.array([0, 5])],
                [np.array([3, 0]), np.array([4,0]), np.array([5, 0])],
                [np.array([3, 3]), np.array([4,4]), np.array([5, 5])],
            ]
        )

        # Ensure imutability
        np.testing.assert_array_almost_equal(old_chains, [
                [np.array([0, 3]), np.array([0,4])],
                [np.array([3, 0]), np.array([4,0])],
                [np.array([3, 3]), np.array([4,4])],
            ])

    def test_new_points(self):
        points = [np.array([5,0]), np.array([0, 5]), np.array([-5, -5])]
        old_chains = [
                [np.array([0, 3]), np.array([0,4])],
                [np.array([3, 0]), np.array([4,0])]
            ]

        new_chains = assign_points_to_chains(points, old_chains, lambda x: 2, 4)
        expected = [
                [np.array([0, 3]), np.array([0,4]), np.array([0, 5])],
                [np.array([3, 0]), np.array([4,0]), np.array([5, 0])],
                [np.array([-5, -5])]
            ]


        for (i, chain) in enumerate(new_chains):
            np.testing.assert_array_almost_equal(chain, expected[i])

    def test_missing_point(self):
        points = [np.array([5,0])]
        old_chains = [
                [np.array([0, 3]), np.array([0,4])],
                [np.array([3, 0]), np.array([4,0])]
            ]

        new_chains = assign_points_to_chains(points, old_chains, lambda x: 2, 4)
        expected = [
                [np.array([0, 3]), np.array([0,4]), None],
                [np.array([3, 0]), np.array([4,0]), np.array([5, 0])],
            ]

        self.compare_chains(new_chains, expected)


    def test_distant_matches_get_none(self):
        points = [np.array([5,0])]
        old_chains = [
                [np.array([0, 3]), np.array([0,4])],
            ]

        new_chains = assign_points_to_chains(points, old_chains, lambda x: 2, 4)
        expected = [
                [np.array([0, 3]), np.array([0,4]), None],
                [np.array([5,0])]
            ]

        self.compare_chains(new_chains, expected)

    def test_long_none_chains_get_trimmed(self):

        points = [np.array([5,0])]
        old_chains = [
                [np.array([0, 3]), np.array([0,4]), None, None],
            ]

        new_chains = assign_points_to_chains(points, old_chains, lambda x: 2, 2)
        expected = [
                [np.array([5,0])]
            ]

        self.compare_chains(new_chains, expected)



    def compare_chains(self, chains, expected_chains):
        for (i, chain) in enumerate(chains):
            for (j, point) in enumerate(chain):
                expected = expected_chains[i][j]
                if expected is None:
                    if point is not None:
                        raise AssertionError("Expected None, got " + str(point) + ". chains: " + str(chains) + " expected: " + str(expected_chains))
                else:
                    if not (point == expected).all():
                        raise AssertionError("Expected " + str(expected) + " got " + str(point) + ". chains: " + str(chains) + " expected: " + str(expected_chains))




if __name__ == "__main__":

    unittest.main()

